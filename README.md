# AWS

> AWS resources.


* https://aws.amazon.com/deepracer/
* https://aws.amazon.com/robomaker/
* https://aws.amazon.com/sagemaker/
* https://aws.amazon.com/solutions/media-analysis-solution/
* ...
* https://github.com/aws-samples/aws-deepracer-workshops
* https://github.com/aws-samples/amazon-lex-customerservice-workshop
* https://github.com/awslabs/media-analysis-solution
* https://s3.amazonaws.com/mae303/MediaAnalysisWorkshop.pdf
* https://github.com/aws-samples/amazon-personalize-samples
* https://github.com/aws-samples/amazon-forecast-samples
* https://www.kaggle.com/prajitdatta/movielens-100k-dataset
* https://storage.googleapis.com/openimages/web/index.html
* ...
* https://github.com/awslabs/amazon-sagemaker-examples
* https://github.com/aws-samples/amazon-sagemaker-keras-text-classification
* https://github.com/awslabs/handwritten-text-recognition-for-apache-mxnet
* https://github.com/aws-samples/amazon-textract-enhancer
* https://github.com/aws-samples/amazon-sagemaker-deepar-retail
* https://github.com/aws-samples/SageMaker_seq2seq_WordPronunciation
* ...
* https://github.com/openai/gpt-2
* https://github.com/minimaxir/textgenrnn
* https://github.com/jcjohnson/torch-rnn
* ...



### TBD

Delete all media-analysis sample resources



